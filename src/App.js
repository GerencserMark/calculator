import { useState } from 'react';

function App() {
  const [calc, setCalc] = useState("");
  const [result, setResult] = useState("");

  const ops = ['+','-','*','/','.'];

  const updateCalc = value => {
    if(
      ops.includes(value) && calc === '' ||               // if last value is an operator and calc is nothing
      ops.includes(value) && ops.includes(calc.slice(-1)  // value is operator and last value is also an operator
      )
    ) {
      return;                                             // return and not do anything
    }
    setCalc(calc + value);

    if (!ops.includes(value)) {
      setResult(eval(calc + value).toString());
    }
  }

  const createDigits = () => { //1-tol 9-ig szamok letrehozasa buttonoknak
    const digits = [];

    for (let i = 1; i < 10; i++){
      digits.push(
        <button 
            onClick={() => updateCalc(i.toString())} 
            key={i}>
            {i}
        </button>
      )
    }
    return digits;
  }

  const calculate = () => {
    setCalc(eval(calc).toString());
  }

  const clear = () => {
    setCalc("0");
  }

  const deleteLast = () => {
    if (calc == ''){
      return;
    }
    const value = calc.slice(0, -1);

    setCalc(value);
  }

// if there is a result, display it, if not then show nothing "0" ({ result ? <span>(0)</span> : '' } { calc || "0" })
  return (
    <div className="App">
      <div className="calculator">
        <div className="display">
          { result ? <span>({result})</span> : '' } { calc || "0" } 
        </div>

        <div className="operators">
          <button onClick={() => updateCalc('+')}>+</button>
          <button onClick={() => updateCalc('-')}>-</button>
          <button onClick={() => updateCalc('*')}>*</button>
          <button onClick={() => updateCalc('/')}>/</button>

          <button onClick={deleteLast}>DEL</button>
          <button onClick={clear}>C</button>
        </div>

        <div className="digits">
          { createDigits() }
          <button onClick={() => updateCalc('0')}>0</button>
          <button onClick={() => updateCalc('.')}>.</button>
          <button onClick={calculate}>=</button>
        </div>

      </div>
    </div>
  );
}

export default App;
